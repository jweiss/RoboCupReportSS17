%\appendix

\chapter{Appendix}
  
\section{Debug of the Self Localization}
\label{sec:debug-self-locator}
An important part of our work was done thanks to the understanding of the Self Localization Debugging tools. Page 138 of \cite{BHumanCodeRelease2013} sheds some light on this aspect. Two basic commands used on the SimRobot console are \code{vfd} and \code{vid}.\\ 

\code{vfd} calls a debug drawing in a field view.

\begin{lstlisting}
vfd worldState <command>
vfd worldState module:SelfLocator:simples
\end{lstlisting}

\code{vid} calls a debug drawing for an image.

\begin{lstlisting}
vid <image name> <command>
vid segmentedUpper representation:LinePercept:Image
\end{lstlisting}

Plot the pose of every particle as a yellow rectangle and with a line representing its orientation:

\begin{lstlisting}
vfd worldState module:SelfLocator:simples
\end{lstlisting}

Plot the variance matrices of the samples represented as ellipses on the worldState:

\begin{lstlisting}
vfd worldState module:SelfLocator:samples
\end{lstlisting}

Project the current camera calibration on the field to see if it matches reality: 
\begin{lstlisting}
vid upper module:CameraMatrixProvider:calibrationHelper
\end{lstlisting}

To see lines, crossings, and detected penalty marks type:
\begin{lstlisting}
vid segmentedUpper representation:LinePercept:Image
vid segmentedUpper representation:LinePercept:ImageText
\end{lstlisting}

To plot your own lines, circles and dots check \textit{Src/Tools/Debugging/DebugDrawings.h}. This head file contains useful debugging functions to plot either on images or on the \textit{worldState} representation.

\begin{lstlisting}
#define DRAWTEXT(id, x, y, fontSize, color, txt)
#define DOT_AS_VECTOR(id, xy, penColor, brushColor)
#define LINE(id, x1, y1, x2, y2, penWidth, penStyle, penColor)
#define ARROW(id, x1, y1, x2, y2, penWidth, penStyle, penColor)
#define POSE_2D_SAMPLE(id, p, color)
\end{lstlisting}

An example of a custom debugging function declared in a \code{cpp} file is show below. To declare a custom drawing function use the token: \code{DECLARE\_DEBUG\_DRAWING} with its respective parameters. Right after this command you can put a simple plot, for instance a line, or declare a \code{COMPLEX\_DRAWING}. \textit{module:SelfLocator:drawProjection} is the name which would be called with a debugging function. For printing on the field use \textit{drawingOnField}.

\begin{lstlisting}
// Debugging tool for the line verification
DECLARE_DEBUG_DRAWING("module:SelfLocator:drawProjection", 
                      "drawingOnImage");
COMPLEX_DRAWING("module:SelfLocator:drawProjection")
{
  int counter = 0;
  for(vector<float>::const_iterator it = ill.begin(); it != ill.end(); ++it){

    float illumination(*it);
    DRAWTEXT("module:SelfLocator:drawProjection", 
             166, 
             0 + 10* counter, 
             4,
             ColorRGBA::blue, 
             ""<< illumination);
    
    counter++;
   }

   DRAWTEXT("module:SelfLocator:drawProjection", 190, 0 , 4, ColorRGBA::red, ""<< index_of_max);
}
\end{lstlisting}

Since we said it will be \textit{drawingOnImage} our plot would be seen for a call like:

\begin{lstlisting}
vid upper module:SelfLocator:drawProjection
\end{lstlisting}

\section{Important Configuration Files}

In order to implement different situations and to test our algorithms, we had to change some lines of code which are not intuitively connected. These lines allow modifications on the field dimensions, number of particles, intial position of particles for the SelfLocator, initial position of the robot in SimRobot, robot role in SimRobot among many others. 

A really useful simulation among all is \textit{SingleRobot.ros2} in combination with \textit{Keeper.h}, which helped us implement a straigh line walk as well as other useful situations for debugging the Self Localization Algorithm.

\subsection*{Field Dimensions}

\code{Config/Locations/Default/FieldDimensions.cfg}

This file is important if you want to simulate the field or even for testing on real conditions. The field found in the ICS uses \code{yPosLeftSideline = 1500} and the official RoboCup setup is \code{yPosLeftSideline = 3000}.\code{3000} should also be used in SimRobot simulations, unless you want to change the ros file which describes the field: \code{/Config/Scenes/Includes/Field2015SPL.rsi2} .You do not need to change any other parameters, since the right side is simmetrically adjusted in lines like \code{yPosRightSideline = -yPosLeftSideline} For a better visualization of the green rectangle, you should also want to adjust yPosLeftFieldBorder to a value bigger than \code{3000} in other to make is coherent with reality in SimRobot's worldState. 

Field size is critical for the Self Localization Algorithm, since the white lines found will be matched to a model of the field defined in this file. If the robot expects a \code{yPosLeftSideline} to be \code{3000} but you are on the ICS field, you should not be surprised if the localization does not work as expected.

\begin{lstlisting}
 ...
 yPosLeftFieldBorder = 1900; // Change if needed

 yPosLeftSideline = 1500; // Change if needed
 yPosLeftDropInLine = 1300;
 yPosLeftPenaltyArea = 1100;
 yPosLeftGoal = 800;
 yPosCenterGoal = 0;
 yPosRightGoal = -yPosLeftGoal;
 yPosRightPenaltyArea = -yPosLeftPenaltyArea;
 yPosRightDropInLine = -yPosLeftDropInLine;
 yPosRightSideline = -yPosLeftSideline;
 yPosRightFieldBorder = -yPosLeftFieldBorder;
 ... 
\end{lstlisting}

\subsection*{Intial Pose of particles for the SelfLocator}

\code{Src/Modules/Modeling/SelfLocator/TemplateGenerator.cpp}

Some interesting setups would need to modify the initial pose of particles, either to accelerate the Self Localization or to test an expected behavior. In the case of the Line Verification Algorithm we needed the first predition to be located exactely on the crossing between midfield line an side line. Be aware that there are 5 Roles, so when you instatiate a robot this in the simulation or by copying files, this numbers have to match.

\begin{lstlisting}
  Vector2f pos1(-3000.f, theFieldDimensions.yPosLeftSideline);
  Vector2f pos2(-1000.f, theFieldDimensions.yPosLeftSideline);
  Vector2f pos3(-2000.f, theFieldDimensions.yPosRightSideline);
  Vector2f pos4(-2000.f, theFieldDimensions.yPosLeftSideline);
  Vector2f pos5(-1000.f, theFieldDimensions.yPosRightSideline);
  Vector2f centerOfGoal(theFieldDimensions.xPosOwnGroundline, 0.f);  
  Vector2f pos1ToGoal = centerOfGoal - pos1;
  Vector2f pos2ToGoal = centerOfGoal - pos2;
  Vector2f pos3ToGoal = centerOfGoal - pos3;
  Vector2f pos4ToGoal = centerOfGoal - pos4;
  Vector2f pos5ToGoal = centerOfGoal - pos5;
  float angle1 = std::atan2(pos1ToGoal.y(), pos1ToGoal.x());
  float angle2 = std::atan2(pos2ToGoal.y(), pos2ToGoal.x());
  float angle3 = std::atan2(pos3ToGoal.y(), pos3ToGoal.x());
  float angle4 = std::atan2(pos4ToGoal.y(), pos4ToGoal.x());
  float angle5 = std::atan2(pos5ToGoal.y(), pos5ToGoal.x());
  walkInPositions.push_back(Pose2f(angle1, pos1)); // Robot 1
  walkInPositions.push_back(Pose2f(angle2, pos2)); // Robot 2
  walkInPositions.push_back(Pose2f(angle3, pos3)); // Robot 3
  walkInPositions.push_back(Pose2f(angle4, pos4)); // Robot 4
  walkInPositions.push_back(Pose2f(angle5, pos5)); // Robot 5
\end{lstlisting}

\subsection*{Intial Pose and Role of Robot in SimRobot}

\code{Config/Scenes/SinglePlayer.ros2}

In case you want to spare time by positioning the robot in a give set of coordinates on the field, you can modify \code{name}, \code{Translation} and \code{Rotation}.

\begin{lstlisting}
    <Compound name="robots">
      <Body ref="Nao" name="robot4">
        <Translation x="-2.0" y = "3" z="320mm"/>
        <Rotation z="180degree"/>
        <Set name="NaoColor" value="blue"/>
      </Body>
    </Compound>
\end{lstlisting}

\subsection*{Robot role and robot number}

Robot roles are merely defined by the robot name called in SimRobot or even while copying files:\\

robot1 is a red goalkeeper\\
robot2 is a red defender\\
robot3 is a red striker\\

robot7 is a blue goalkeeper\\
robot8 is a blue defender\\
robot11 is a blue striker\\

%\code{./copyfiles Develop \<IP\_AAA.BBB.CCC.DDD\> -p 3 -r}

\code{\small cd <Bhuman-CodeRelease/Make/Linux>}\\
\code{\small ./copyfiles Develop <IP\_AAA.BBB.CCC.DDD> -p 3}\\

In this case we want our robot to play as a red striker.

\subsection*{Number of Samples and other interesting parameters for SelfLocator}

\code{Config/Locations/Default/selfLocator.cfg}

While debugging the SelfLocator you will probably come across with some parameters which could be adjusted for special needs. This can be done in this file.

\begin{lstlisting}
 numberOfSamples = 12;
 ...
 goalAssociationMaxAngle = 0.25; // rad
 goalAssociationMaxAngularDistance = 0.02; // rad
 centerCircleAssociationDistance = 3000.0;
 penaltyMarkAssociationDistance = 2000.0;
 lineAssociationCorridor = 300.0;
 cornerAssociationDistance = 500.0; 
 centerCircleGoalPostMaxDistanceDiff = 1000.0;
 ...
 alwaysAssumeOpponentHalf = false;
 goalFrameIsPerceivedAsLines = true;
 minGroundLineLength = 1500.0;  
 maxGoalPostDistFromGroundLine = 400.0;
 baseValidityWeighting = 0.1;
 goalieFieldBorderDistanceThreshold = 2500.0;
 goalieNoPerceptsThreshold = 8000.0;
 goalieJumpTimeout = 15000;
 positionJumpNotificationDistance = 2000.0;
\end{lstlisting}


\section{Create Log Files }

The Bhuman framework offers an opportunity to log with a remote connection all data like images, joint values and odometry data from the Nao robot. This function is called ''Remote Logging''. Through this the complete sensor data and joint data can be replayed afterwords. Also new algorithm can be analysed with the logged data. This saves much test time on the real field. Also algorithms can be well analysed and compared in the behaviour on the real field. While sending large data like the video stream with the remote connection the real-time capability can not be guaranteed. The tool SimRobot from the Bhuman team is needed to start a log file. The following steps are required for the remote logging\cite{BHumanCodeRelease2016}:


\subsection*{Pre Configuration}

In the pre configuration step the representations, which should be logged, must be selected. This is done in the configuration file ''logger.cfg''. To debug and replay for example the self locator the following representations are needed:

\begin{lstlisting}
ActivationGraph,
BallModel,
BallPercept,
BehaviorMotionRequest,
BodyContour,
CameraInfo,
CameraMatrix,
FieldBoundary,
FrameInfo,
GameInfo,
GoalPercept,
ImageCoordinateSystem,
LinePercept,
LineSpots,
LocalizationTeamBall,
MotionRequest,
ObstacleModel,
OdometryData,
OwnTeamInfo,
PenaltyMarkPercept,
PlayersPercept,
RobotHealth,
RobotInfo,
RobotPose,
ScanlineRegions,
SideConfidence,
TeamBallModel,
TeamPlayersModel,
TeammateData,
Thumbnail
\end{lstlisting}

The variable ''\textit{maxBufferSize}'', which can be found in the configuration file, defines the maximal length in seconds of the log data file.

\section*{Logging in SimRobot}

To log data from the robot in SimRobot we connect with the configured ''\textit{RemoteRobot.con}'' profile. The debug output is enabled on the NAO with the following commands


\begin{lstlisting}
dr off
dr representation:Image
dr representation:JointAngles
dr representation:JointSensorData
dr representation:CameraMatrix
dr representation:ImageCoordinateSystem
dr representation:CameraInfo
dr representation:OdometryData
\end{lstlisting}

The commands for the log file are
\begin{lstlisting}
log start
log stop
log save fileName
\end{lstlisting}

The activation of the logging can be controlled with the counter ''recoreded number'' on the left bottom of the SimRobot window.

\subsection*{Replay of Log Files}

The log files can be also replayed within SimRobot. With the profile \textit{ReplayRobot.con} the recorded log files, which are located in \textit{Config/Logs}, can be opened. The following commands can be used to control the replay of the log file:
\begin{lstlisting}
Ctrl+Shift+A: Pause replay
Ctrl+Shift+S: Start/Continue replaying the log
Ctrl+Shift+D: Goto first frame
Ctrl+Shift+F: Repeat current frame
Ctrl+Shift+Y: One frame backward
Ctrl+Shift+X: One frame forward
Ctrl+Shift+C: Jump to previous frame that contains an image
Ctrl+Shift+V: Jump to next frame that contains an image
Ctrl+Shift+B: Jump backward 100 frames
Ctrl+Shift+N: Jump forward 100 frames
\end{lstlisting}

\section{Estimation Setting-up of Particle Filter}
The initial setting-up of estimation particle filter can be changed in file ``TemplateGenerator.cpp". This file locates in Modeling/SelfLocator. In this file we can change the position and orientation of initial particle filter estimation.

\subsection*{Vector2f pos4}
This parameter defines the initial position of estimation, where i is number from 1 to 5 and refers to different roles, for example number 4 represents striker . It consists of two variables, the first is x axes, the other is y axes.

\subsection*{Float angle4}
This parameter defines the orientation of initial estimation. For example, in our strategy, we set angle equal to 0.5*pi.

\begin{lstlisting}
void TemplateGenerator::init()
{
......
Vector2f pos1(-3000.f, theFieldDimensions.yPosLeftSideline);
Vector2f pos2(-1000.f, theFieldDimensions.yPosLeftSideline);
Vector2f pos3(-2000.f, theFieldDimensions.yPosRightSideline);
Vector2f pos4(-2000.f, -theFieldDimensions.yPosLeftSideline);
Vector2f pos5(-1000.f, theFieldDimensions.yPosRightSideline);
Vector2f centerOfGoal(theFieldDimensions.xPosOwnGroundline, 0.f);
Vector2f pos1ToGoal = centerOfGoal - pos1;
Vector2f pos2ToGoal = centerOfGoal - pos2;
Vector2f pos3ToGoal = centerOfGoal - pos3;
Vector2f pos4ToGoal = centerOfGoal - pos4;
Vector2f pos5ToGoal = centerOfGoal - pos5;
float angle1 = std::atan2(pos1ToGoal.y(), pos1ToGoal.x());
float angle2 = std::atan2(pos2ToGoal.y(), pos2ToGoal.x());
float angle3 = std::atan2(pos3ToGoal.y(), pos3ToGoal.x());
float angle4 = 0.5*pi; //*std::atan2(pos4ToGoal.y(), -pos4ToGoal.x());
float angle5 = std::atan2(pos5ToGoal.y(), pos5ToGoal.x());
......
}
\end{lstlisting}





% !TeX spellcheck = en_US
% !TeX encoding = ISO-8859-1
\part{Self Localization}
\chapter{Self Localization Algorithm}\label{c:self_loc}


\section{Overview}

The self localization algorithm of robotic systems and in this case the NAO robots can be divided into six steps. Figure~\ref{fig:SelfLocalization} gives an overview about these steps. In the following chapters these steps will be shortly introduced. 

\begin{figure}[H]
	\centering
		
	\tikzstyle{block} = [rectangle, draw, text width=10em, text centered, rounded corners, minimum height=3em]
	\begin{tikzpicture}
	[node distance=1.6cm,
	start chain=going below,]
	\node (n1) at (0,0) [block] {Image Capture};
	\node (n2) [block, below of=n1] {Segmentation};
	\node (n3) [block, below of=n2] {Blob Forming and Object Recognition};
	\node (n4) [block, below of=n3] {Distance Calculation and Filtering};
	\node (n5) [block, below of=n4] {Detecting the goal and lines};
	\node (n6) [block, below of=n5] {Particle Filter};
	% Connectors
	\draw [->,line width=1pt] (n1) -- (n2);
	\draw [->,line width=1pt] (n2) -- (n3);
	\draw [->,line width=1pt] (n3) -- (n4);
	\draw [->,line width=1pt] (n4) -- (n5);
	\draw [->,line width=1pt] (n5) -- (n6);
	\end{tikzpicture}
	\caption[Overview about the self localization of the NAO robot]{The self localization algorithm of the NAO robot persists out of six parts.}
	\label{fig:SelfLocalization}
\end{figure} 



\section{Image Capture}

Every type of self localization system needs in the first step some kind of sensor data. The NAO robot gets information about its surrounding environment with the help of two cameras. They are located on the head and provide images with a maximum resolution of VGA (640 x 480 pixels) with maximum 30 fps. Figure~\ref{fig:hardwarecameraposition} shows the position of the cameras with their view angles. 

\begin{figure} [H]
	\centering
	\includegraphics[width=0.5\linewidth]{pics/hardware_cameraposition}
	\caption[NAO camera]{The NAO robot has two cameras, which are placed on the head.~\cite{Aldebaran}}
	\label{fig:hardwarecameraposition}
\end{figure}

Besides the motion of the body (arms and legs), the head motion is handled separately\cite{BHumanCodeRelease2013}. The head control is an independent option parallel to body control, which controls the two head joint angles, as shown in \fref{24}, namely pitch and yaw movements. During the game, the robot camera on its head should keep tracking of the robot's position, ball's position. Except the angle control, speed constraints of head movement must be taken into consideration during designing. On the one hand, too fast head movement will result in blurred captured images. On the other hand, a high speed of head movement will have a negative influence of the body balance. \\
In\cite{UP}, a Head Finite State Machine (HeadFSM) is designed by the UPennalizers - University of Pennsylvania Robot Soccer Team. Each state is designed for different need and different conditions determine when to switch into ball searching, ball tracking, and simply looking around. And also in B-Human team, a relative complex head modes has been implemented. The current algorithm uses just one state lookLeftAndRight.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.4\textwidth]{pics/24}
	\caption{Nao Head has two degree of freedom}
	\label{24}
\end{figure}

\section{Segmentation}

In order to make computers capable to detect objects in an image, it is necessary to segment the image first. Therefore in the first step a color classification is executed. The Bhuman framework uses as in~\cite{BHumanCodeRelease2013} described, a look up table, which is based on the HSV color space. The thresholds for each color must be defined in a prior step with the help of the color calibration~\cite{robotumwiki}. With the result of the color classification a color segmentation can be done like in the figure~\ref{fig:colorsegmentation} shown.
\begin{figure} 
	\centering
	\includegraphics[width=0.7\linewidth]{pics/ColorSegmentation}
	\caption[Color Segmentation]{In the first line is the original image and in the second line the results from color segmentation (left white, middle orange and right green)~\cite{Keirsbilck2016}.}
	\label{fig:colorsegmentation}
\end{figure}

 
\section{Blob Forming and Object Recognition}
 
In the blob forming and object recognition step we detect on the segmented image features like the ball, other robots and lines. The blob forming process is done in an efficient way with the help of vertical scanlines~\cite{BHumanCodeRelease2015}. 

\section{Distance Calculation and Filtering}

The next step of the self localization algorithm is the distance calculation to the features, which got extracted in the previous step.
\begin{figure} [H]
	\centering
	\includegraphics[width=0.5\linewidth]{pics/distanceCalculation}
	\caption[Distance Calculation]{In the ''Distannce Calculation anf Filtering'' step the distance from the robot to the features is calculated~\cite{Sanchez2013}.}
	\label{fig:distancecalculation}
\end{figure}

Figure~\ref{fig:distancecalculation} shows the geometrical approach. To achieve correct calculations a filtering process is used. As the first step of the filtering the lens distortions are compensated. These distortions are caused through the curvature of the camera lenses. The second step of the filtering process is an extrinsic adjustment, which is based on the position of the camera. This adjustment describes the coordinate transformation between the robot and the feature~\cite{Sanchez2013}. The parameters for the transformation can be achieved with the ''CameraCalibration'' module from the B-Human framework~\cite{BHumanCodeRelease2016}.


\section{Distance Calculation and Filtering}

In the next step a local model of the game space is developed. For this purpose the goal and lines are search out of all the features from the last step. To avoid noise through locomotion vibrations and bounces while walking, a moving average filter for filtering the calculated distance is added. To cancel the effect of a delay caused through this filter type, a prediction step is added to the filter~\cite{Sanchez2013}. 


\section{The Particle Filter Algorithm }
\label{sec:particle_filter_algorithm}
The particle filter algorithm is a typical statistical approach for localization problems in robotics. It is based on the principle of discretization of a complex probability density function. The given set of particles tries to represent this complex distribution. In this case Unscented Kalman Filters are used to track the pose of every particle in the field. The current implementation of the algorithm uses 12 particles. Normally, this number should be way higher than a dozen, but since our robot's computational capacity is limited, we are bounded to this quantity.\\

The Unscented Kalman Filter is used for processes with high non linearities, as in a RoboSoccer game: uneven steps, non linear friction, crashes with other robots, etc. In SelfLocator.cpp an initialization can be seen. All 12 particles are initialized to a position given in the file TemplateGenerator.cpp and depend on the assigned robot number, which can be also interpreted as the robot role. The following code snippet makes clear that every single particle is initialized with the same pose. An initial spreading of the particles should be of use and may be worth a try. \\
\code{\small Src/Modules/Modeling/SelfLocator/TemplateGenerator.cpp}
\begin{lstlisting}
samples = new SampleSet<UKFSample>(numberOfSamples);
for(int i=0; i<samples->size(); ++i)
{
Pose2f pose = sampleGenerator.getTemplateAtWalkInPosition();
samples->at(i).init(pose, *this, nextSampleNumber++);
living_iterations.push_back(0);
}
\end{lstlisting}


The position of each particle is then updated based on changes of the odometry. Displacements which are read from the robot sensors are then added to the particle current pose. These motion measurements are relative to the current robot position. The strongest criteria for a particle to be updated comes from the Perception modules: Line Perception, Goal Perception and Penalty Mark Perception. The localization is as accurate as the information provided by those functions. Every time a new landmark is detected, mean, covariance matrix and validity for every particle are calculated and corrected. At the beginning of a game the uncertainty, represented with big values in the covariance matrix, is big enough for all the particles to cover all possible spots where the robot may be positioned. This effect can be seen in figure~\ref{sec:particle_filter_algorithm_0}. When the actual pose is more evident, given the detected landmarks, uncertainty shrinks and validity values are increased. Those changes are evident in the \textit{worldState} plot. Figure~\ref{sec:particle_filter_algorithm_1} shows how covariance matrices shrinks in the direction of y given an input from the perception module. The landmark found was a straight line which corresponds to the side of the field. So our robot can estimate its position on y but needs another match with its model for shrinking the covariance on the x axis. 


\begin{figure}[H]
	\centering
	\includegraphics[width=0.70\columnwidth]{pics/SelfLocator-start.png} 
	\caption{Initial covariance matrices and positioning of particles.}
	\label{sec:particle_filter_algorithm_0}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.70\columnwidth]{pics/SelfLocator-big_covariance.png} 
	\caption{Distortion of covariance matrices after line detection.}
	\label{sec:particle_filter_algorithm_1}
\end{figure}

A really unequivocal perception for self localization is the goal detection. If a goal is seen accurately and the camera calibration is good particles improve their distribution considerably. It happens because there are only two goals on the field and each has two posts which allow a better estimation of the robot pose than a floating straight line on the image. We could confirm that the robot localization was not working well due to a low field of view configured before. A higher view angle allowed a more frequent detection of goals and consequently a much better estimation. An example of this case can be seen in figures~\ref{sec:particle_filter_algorithm_2} and \ref{sec:particle_filter_algorithm_3}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.70\columnwidth]{pics/SelfLocator-Goal2.png} 
	\caption{Example of an accurate goal perception from the initial position.}
	\label{sec:particle_filter_algorithm_2}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.35\columnwidth]{pics/SelfLocator-goal.png} 
	\caption{Unequivocal robot pose estimation thanks to a good goal perception.}
	\label{sec:particle_filter_algorithm_3}
\end{figure}

Understanding what is wrong with the Self Localization can be quite challenging without the required tools. Localization is a process which happens internally in the memory of the NAO. The particles plot were the most useful tool we found. A plot for covariance matrices and particle positions can be activated according to the instructions given in~\ref{sec:debug-self-locator}. Information of interest can also be displayed by modifying the respective \code{draw()} function in \textit{SelfLocator.cpp}.

% !TeX spellcheck = en_US
% !TeX encoding = ISO-8859-1
\chapter{The Line Verification Algorithm}\label{c:line_verification}



\section{Motivation and Problem Description}
\label{sec:MotivationProblemDescription}
\subsection{Experiment}
At the beginning of the experiment the robot stands on the side line at the position of the middle line and he has the task to walk straight forward along the middle line. 


\begin{figure} [H]
	\centering
	\subfigure[]{\includegraphics[width=0.3\linewidth]{pics/bad_lower_part1}}\qquad
	\subfigure[]{\includegraphics[width=0.3\linewidth]{pics/bad_lower_part2}}\\
	\subfigure[]{\includegraphics[width=0.3\linewidth]{pics/bad_lower_part3}}\qquad
	\subfigure[]{\includegraphics[width=0.3\linewidth]{pics/bad_lower_part4}}
	\caption[Outcome of the debug lines during an experiment.]{Outcome of the debug lines for each particle during the performed experiment with the line verification algorithm. The red line is the line model of the used particle.}
	\label{fig:experiment_upper_camera}
\end{figure}

\begin{figure} [H]
	\centering
	\subfigure[]{\includegraphics[width=0.7\linewidth]{pics/bad_upper_part1}}\qquad
	\subfigure[]{\includegraphics[width=0.7\linewidth]{pics/bad_upper_part2}}\\
	\subfigure[]{\includegraphics[width=0.7\linewidth]{pics/bad_upper_part3}}\qquad
	\subfigure[]{\includegraphics[width=0.7\linewidth]{pics/bad_upper_part4}}
	\caption[Outcome of the projected particles during an experiment.]{Outcome of the performed experiment. It can be seen on SimRobot with the help of an remote connection.}
	\label{fig:experiment_particles}
\end{figure}

Each black line in Figure~\ref{fig:experiment_upper_camera} represents the line model of the associated particle. Figure (a) shows the begin of the experiment. Figure (d) shows the end of the experiment and the figures (b) and (c) are taken during the experiment. The red line shows the line model of the actual used particle. We can observe in (b) to (d) that the line models are fitting much worse by the progress of the experiment. \\
Figure~\ref{fig:experiment_particles} shows the ''worldState'' in the debug software SimRobot, which is provided by the B-Human team. This debug drawing shows the position of the robot and of the particles. The yellow rectangles represent the particles and the red edged rectangle represents the best particle, which is simultaneously used as the current position of the robot. The time steps are the same as in Figure~\ref{fig:experiment_upper_camera}. We can observe after some seconds of walking, that the calculated position of the robot is drifting to the left-side. Also the particles are spreading away from each other by the progress of the experiment. It is obvious that the calculated position has nothing to do with the real movement of the robot along the middle line. \\
The experiment motivates to improve the self localization algorithm. Because if this proper easy self localization task with many good white lines on the captured image can not be solved, it is obvious that more difficult situations will have also wrong localization results.

\subsection{The Drifting and Spreading Problem}

The reason for the side drift is the algorithm for walking straight. To let the robot walk a straight line, a rotation bias has to be added. Without this parameter the Nao could only walk a circle. The consequence of this correction paramter is a permanent drift to the left~\cite{Walter2017}. The spreading of the particles is additional caused through the motion and the different rotations of the particles. 

\section{Implementation}

\subsection{Line Verification}
\label{sec:lineVerification}

To implement the line verification algorithm described on \cite{Walter2017} some code had to be reused. As said in the last section, camera calibration lines are the result of a model given in the configuration files and the estimated robot pose. The situation could be reproduced for 12 pose estimations and can be seen in figure~\ref{sec:line_verification_f_1}. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.70\columnwidth]{pics/bad_lower_part1.jpg} 
	\caption{Field lines projected on the image for every single sample.}
	\label{sec:line_verification_f_1}
\end{figure}

After having the lines as objects, a function was written to return an array of sample points separated by a given distance. Every sample was represented in figure~\ref{sec:line_verification_f_2} as a red square. Line verification with relatively high sampling density (every 5 to 10 pixels) worked better.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.70\columnwidth]{pics/pixel_sampling_single.png} 
	\caption{Sampling across the projected lines.}
	\label{sec:line_verification_f_2}
\end{figure}

Figure~\ref{sec:line_verification_f_3} illustrates how points outside the image frame are ignored for the following calculations even though some important lines are completely outside. The constant sampling distance is advantageous since white lines closer to the robot have higher probability of being sampled: If a 1 meter long horizontal line is placed 1 meter away from the robot it will have a length of say 100 pixels in the camera, while the same line placed 5 meters away could be represented in the images as a 20 pixels length line an thereby be sampled just on two spots for a 10 pixel grid. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.70\columnwidth]{pics/pixel_sampling_multiple.png} 
	\caption{Sampling lines for several projections.}
	\label{sec:line_verification_f_3}
\end{figure}

A way to connect \textit{SelfLocator} and \textit{ImagePerception} was created. It will be explained in \ref{sec:line_verification_cod}. The aim of this connection was the reading of pixel intensities for every single sampling point. The value selected was the luma component (Y) in the YCbCr color space. It corresponds to the illumination, which is the variable of interest. 

The next step is calculating the average illumination of every projection. That was done using the following summation:

\begin{equation} 
\bar{I}_P=\frac{1}{N_P}\sum_{i=1}^{N_P}I_{Pi} 
\end{equation}

Where $N_P$ represents the number of pixels sampled for given projection $P \in {\left \{ 1,...,12 \right \}}$. We end up with an array of average intensities for every $P$.

\subsection{Decision Rules for Resetting Particles}

After the calculations of~\ref{sec:lineVerification} we proceed with the promised particle selection. 

The best sample is the one with the highest illumination average. This criterion works specially well after applying a moving average to minimize noisy measurements. 

\paragraph{Particle Resetting}
Particles are not deleted. An array size of 12 samples remains constant for the whole execution of the algorithm. When a particle needs to be reset, its pose is replaced with that of the best particle. A programming issue regarding this topic is explained in \ref{sec:line_verification_cod}.

\subsubsection{Programming details}
\label{sec:line_verification_cod}
A way to access single pixels from \textit{SelfLocator.cpp} was needed. Some methods were directly taken from the \textit{BallPerceptor}, which looks for red color pixels in the image. The required libraries copied to \textit{SelfLocatorBase.h} were:

\begin{lstlisting}
// Needed for the line verification
#include "Representations/Configuration/ColorTable.h"
#include "Representations/Perception/ImageCoordinateSystem.h"
#include "Representations/Perception/BallPercept.h"
#include "Representations/Perception/BallSpots.h"
\end{lstlisting}

A function taken from the \textit{BallPerception} and used in the line verification algorithm:

\begin{lstlisting}
Image::Pixel getPixel(int y, int x)
\end{lstlisting}

Another needed function for the Line Verification

\begin{lstlisting}
bool SelfLocator::projectLineOnFieldIntoImage(
const Geometry::Line& lineOnField, 
const CameraMatrix& cameraMatrix,
const CameraInfo& cameraInfo,
Geometry::Line& lineInImage) const
\end{lstlisting}


When resetting a particle it is always important to recover its id after replacing it with the data saved for the best sample:

\begin{lstlisting}
int old_id = samples->at(j).id;
samples->at(j) = samples->at(index_of_max);
samples->at(j).id = old_id;
\end{lstlisting}



\subsection{Decision Algorithm}
\label{sec:Implementation}

With the calculations of the line verification algorithm~\cite{Rojas} we get for all $P$ particles an average illumination value $\boldsymbol{\overline{I}}=\{\overline{I}_{1}, ..., \overline{I}_{P}\}$. In the next step of the line verification algorithm the particles are evaluated and in case of bad particles reseted. Figure~\ref{fig:DecisionRule} shows the structure of the algorithm, which is based on the results of Michael Walter~\cite{Walter2017}.

\begin{figure} [H]
	\centering
	\includegraphics[width=1\linewidth]{pics/FlowChart}
	\caption[Overview Decision Algorithm]{A particle is reseted, if either the distance to the position of the best particle $\boldsymbol{X}_{\text{max}}$ is larger than the threshold $T_{D}$ or the illumination is smaller than the threshold $T_{I}$ of the illumination $\overline{I}_{max}$ of the best particle. The distance threshold relies also on a life time condition with the threshold $T_t$.}
\label{fig:DecisionRule}
\end{figure}


\paragraph{Illumination Threshold}
In each iteration the algorithm compares the average illumination value of the best particle $\overline{I}_{max}=\max(\boldsymbol{\overline{I}})$ with the average illumination values $\boldsymbol{\overline{I}}=\{\overline{I}_{1}, ..., \overline{I}_{P-1}\}$ of the remaining $P-1$ particles. If $\overline{I}_{p}$ of the $p$-particle is smaller than the value of the best particle $\overline{I}_{max}$ multiplied with the threshold $T_{I}$, this particles is reseted. This means in conclusion, that the line model of the $p$-particle, which is reseted, fits much worse to the image of the real field as the line model of the particle $\overline{I}_{max}$.

\paragraph{Distance Threshold}
In the second part of the distance algorithm of the line verification algorithm, the position $\boldsymbol{X}_{\text{max}}$ of the best particle with the corresponding illumination value $\overline{I}_{max}$ is determined. Based on this position the euclidean distance $\|\boldsymbol{X}_{\text{p}}-\boldsymbol{X}_{\text{max}}\|_{2}$ between all $P-1$ particles and the best particle is calculated. If the euclidean distance is larger than the threshold $T_{D}$, this particle should be reseted.\\
But single particles, which have not fulfilled the first ''Illumination Threshold'' condition and accordingly represent likely a valid mapping of their line model with the real world, should not be directly reseted. The reason therefor is that these particles can be right but for example mirrored.\\ 
To cover this case we implemented a ''life time condition''. Through this condition each particle can be reseted with the distance threshold only after defined iterations $T_t$. This condition does not influence the ''illumination threshold''. 

\paragraph{Reset and New Initialization}
The particle, which should be reseted, is deleted. And to always have a constant number of $P$ particles, a new particle is initialized with the position and angle of the best particle with the average illumination $\overline{I}_{max}$. 
\newpage
\section{Results}\label{s:results}

To prove the influence of the line verification algorithm with the decision algorithm, we performed the same experiment as described in~\ref{sec:MotivationProblemDescription}. The task of the robot is again to walk a straight line.

\begin{table}[H]
	\centering
	\caption{Experimental parameters}
	\label{tb:experimental_parameters}
\begin{tabular}{ll}

	\hline
	& \\
	$alpha$ & 	$0.90$ \\
		 $sample\_distance$ & $5~\text{px}$ \\
		$T_{I}$ & $0.95~\%$ \\
	$T_{D}$ & $200~\text{mm}$ \\
	 $T_t$ & $5$ \\
	Count of Particles $P$ & $12$ \\
	 	& \\
	\hline
\end{tabular}
\end{table}


Table~\ref{tb:experimental_parameters} shows the parameters of this experiment. They are determined heuristic in experiments. $alpha$ is the weighting factor of the moving average filter of the line verification algorithm. A large value of $alpha$ correspondents to a high weight on the results from the previous step. The parameter $sample\_distance$ describes the distance between the sampling points on the lines, which are needed to calculate the average illumination~\cite{Rojas}. The parameters $T_{I}$, $T_{D}$, $T_{t}$ are the thresholds like in section~\ref{sec:Implementation} described. Due to computational costs the Bhuman framework uses only $P=12$ particles for the particle filter~\cite{BHumanCodeRelease2016}. \\ 




 
Figure~\ref{fig:results_particles} shows the projection of the particles in the field in the remote software SimRobot. In compare to the experiment of section~\ref{sec:MotivationProblemDescription} without the line verification algorithm, with the new algorithm the particles stay clustered in a narrow area together. Furthermore the estimated robot position as well as the particles follow the middle line. Also the side drift to the left is limited. \\
The figure~\ref{fig:results_upper_camera} shows the results of the debug line projection for each particle in the image of the upper camera. It can be seen that the projected field lines stay well together and also fit better to the real field lines.

\begin{figure}[H]
	\centering
	\subfigure[]{\includegraphics[width=0.7\linewidth]{pics/good_upper_part1}}\qquad
	\subfigure[]{\includegraphics[width=0.7\linewidth]{pics/good_upper_part2}}\\
	\subfigure[]{\includegraphics[width=0.7\linewidth]{pics/good_upper_part3}}\qquad
	\subfigure[]{\includegraphics[width=0.7\linewidth]{pics/good_upper_part4}}
	\caption[Outcome of the projected particles during an experiment with the line verification algorithm.]{Outcome of the performed experiment with the line verification algorithm. It can be seen on SimRobot with the help of an remote connection.}
	\label{fig:results_particles}
\end{figure}

\begin{figure} [H]
	\centering
	\subfigure[]{\includegraphics[width=0.3\linewidth]{pics/good_lower_part1}}\qquad
	\subfigure[]{\includegraphics[width=0.3\linewidth]{pics/good_lower_part2}}\\
	\subfigure[]{\includegraphics[width=0.3\linewidth]{pics/good_lower_part3}}\qquad
	\subfigure[]{\includegraphics[width=0.3\linewidth]{pics/good_lower_part4}}
	\caption[Outcome of the debug lines during an experiment with the line verification algorithm.]{Outcome of the debug lines for each particle during the performed experiment with the line verification algorithm. The red line is the line model of the best particle.}
	\label{fig:results_upper_camera}
\end{figure}

\section{Conclusion and Future Work}

The experiment showed that the line verification algorithm is a big improvement for the self localization algorithm. But the experiment neglects the case of other robots in the field. Through other robots the illumination average value will get much worse also for probably good particle. This can be one topic for future work to improve the algorithm to be able to consider already detected robots to the calculation of the illumination average value.\\
Furthermore the line verification algorithm should replace the line detection algorithm in future. Because at the moment the algorithm only runs on top of the whole framework. A better implementation besides could save computational effort.~\cite{Walter2017} \\


In general an improvement of the self localization can be done in future with the help of external features. In our lab we have for example an blue wall. In case the robot detects the blue wall, the rotation on the field to the wall as well as the distance to the wall can be calculated. These results would help to get a better result of the self localization. 



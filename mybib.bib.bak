% Encoding: UTF-8
@Misc{BHumanCodeRelease2015,
    author = {Thomas R{\"o}fer and Tim Laue and Jesse Richter-Klug and Maik Sch{\"u}nemann and Jonas Stiensmeier and Andreas Stolpmann and Alexander St{\"o}wing and Felix Thielke},
    title = {{B}-{H}uman Team Report and Code Release 2015},
    year = {2015},
    note = {Only available online: \url{http://www.b-human.de/downloads/publications/2015/CodeRelease2015.pdf}}
}

@Misc{officialrules,
  author = {RoboCup},
  title  = {Standard Platform League (NAO) Rule Book},
  year   = {2016},
  url    = {http://www.tzi.de/spl/pub/Website/Downloads/Rules2016.pdf},
}

@Misc{Aldebaran,
  author = {Aldebaran},
  title  = {NAO Documentation},
  note   = {\url{http://doc.aldebaran.com/1-14/index.html}},
}

@Misc{BHumanCodeRelease2013,
  author = {Thomas R{\"o}fer and Tim Laue and Judith M{\"u}ller and Michel Bartsch and Malte Jonas Batram and Arne B{\"o}ckmann and Martin B{\"o}schen and Martin Kroker and Florian Maa{\ss} and Thomas M{\"u}nder and Marcel Steinbeck and Andreas Stolpmann and Simon Taddiken and Alexis Tsogias and Felix Wenk},
  title = {B-Human Team Report and Code Release 2013},
  year = {2013},
  note = {Only available online: \url{http://www.b-human.de/downloads/publications/2013/CodeRelease2013.pdf}}
}
@article{Rules,
  title={RoboCup standard platform league (nao) rule book, 2016},
  author={RoboCup Technical Committee and others},
  note = {Only available online: \url{www.tzi.de/spl/pub/Website/Downloads/Rules2016.pdf}}
}
@Misc{BHumanCodeRelease2016,
	author = {Thomas R{\"o}fer and Tim Laue and Jonas Kuball and Andre L\"ubken and Florian Maa{\ss} and Judith M\"uller and Lukas Post and Jesse Richter-Klug and Peter Schulz and Andreas Stolpmann and Alexander St{\"o}wing and Felix Thielke},
	title = {{B}-{H}uman Team Report and Code Release 2016},
	year = {2016},
	note = {Only available online: \url{http://www.b-human.de/downloads/publications/2016/CodeRelease2016.pdf}}
}

@Misc{Sanchez2013,
  author                 = {Eduardo~Munera S\'{a}nchez and Manuel~Mu\'{n}oz Alcobendas and Juan Fco~Blanes Noguera and Gin\'{e}s~Benet Gilabert and Jos\'{e}~E. Sim\'{o}~Ten.},
  title                  = {A Reliability-Based Particle Filter for Humanoid Robot Self-Localization in RoboCup Standard Platform League},
  month                  = {Nov},
  year                   = {2013},
  article-doi            = {10.3390/s131114954},
  article-pii            = {sensors-13-14954},
  electronic-issn        = {1424-8220},
  electronic-publication = {20131104},
  history                = {2013/10/29 [accepted]},
  issue                  = {11},
  journal                = {Sensors (Basel, Switzerland)},
  language               = {eng},
  pages                  = {14954-83},
  source                 = {Sensors (Basel). 2013 Nov;13(11):14954-83. doi:10.3390/s131114954.},
  title-abbreviation     = {Sensors (Basel)},
  volume                 = {13},
}

@Misc{robotumwiki,
  author = {RoboTUM},
  title  = {RoboSoccer wiki},
  note   = {\url{https://gitlab.lrz.de/ga68hom/RoboCup/wikis/home}},
}

@Misc{Keirsbilck2016,
  author = {Matthijs Van Keirsbilck and Roland Schmid and Moritz Berthold and Ricardo Vasquez and Ingrid Ibarra and Robert Darius and Oscar Wellenstam},
  title  = {RoboTUM Report SS 2016},
  month  = {August},
  year   = {2016},
}

@Article{Kitano1998,
  author  = {Hiroaki Kitano and Minoru Asada},
  title   = {The RoboCup humanoid challenge as the millennium challenge for advanced robotics},
  journal = {Advanced Robotics},
  year    = {1998},
  volume  = {13},
  number  = {8},
  pages   = {723-736},
  doi     = {10.1163/156855300X00061},
  eprint  = {http://dx.doi.org/10.1163/156855300X00061},
  url     = { 
        http://dx.doi.org/10.1163/156855300X00061
    
},
}

@Article{Walter2017,
  author = {Michael Walter},
  title  = {Revision of the NAO Robot Self-Localization},
  year   = {2017},
}

@Comment{jabref-meta: databaseType:bibtex;}
